apt-get install -y openswan xl2tpd ppp
apt-get install -y lsof

iptables --table nat --append POSTROUTING --jump MASQUERADE
echo "net.ipv4.ip_forward = 1" |  tee -a /etc/sysctl.conf
echo "net.ipv4.conf.all.accept_redirects = 0" |  tee -a /etc/sysctl.conf
echo "net.ipv4.conf.all.send_redirects = 0" |  tee -a /etc/sysctl.conf
for vpn in /proc/sys/net/ipv4/conf/*; do echo 0 > $vpn/accept_redirects; echo 0 > $vpn/send_redirects; done
sysctl -p

echo "for vpn in /proc/sys/net/ipv4/conf/*; do echo 0 > $vpn/accept_redirects; echo 0 > $vpn/send_redirects; done"  |  tee -a /etc/rc.local
echo "iptables --table nat --append POSTROUTING --jump MASQUERADE"  |  tee -a /etc/rc.local

echo "config setup
    dumpdir=/var/run/pluto/
    #in what directory should things started by setup (notably the Pluto daemon) be allowed to dump core?
    nat_traversal=yes
    #whether to accept/offer to support NAT (NAPT, also known as "IP Masqurade") workaround for IPsec
    virtual_private=%v4:10.0.0.0/8,%v4:192.168.0.0/16,%v4:172.16.0.0/12,%v6:fd00::/8,%v6:fe80::/10
    #contains the networks that are allowed as subnet= for the remote client. In other words, the address ranges that may live behind a NAT router through which a client connects.
    protostack=netkey
    #decide which protocol stack is going to be used.

conn L2TP-PSK-NAT
    rightsubnet=vhost:%priv
    also=L2TP-PSK-noNAT

conn L2TP-PSK-noNAT
    authby=secret
    #shared secret. Use rsasig for certificates.
    pfs=no
    #Disable pfs
    auto=add
    #start at boot
    keyingtries=3
    #Only negotiate a conn. 3 times.
    ikelifetime=8h
    keylife=1h
    type=transport
    #because we use l2tp as tunnel protocol
    left=%SERVERIP%
    #fill in server IP above
    leftprotoport=17/1701
    right=%any
    rightprotoport=17/%any" > /etc/ipsec.conf



    ipsec verify


    echo "[global]
ipsec saref = yes

[lns default]
ip range = 172.16.1.30-172.16.1.100
local ip = 172.16.1.1
refuse pap = yes
require authentication = yes
ppp debug = yes
pppoptfile = /etc/ppp/options.xl2tpd
length bit = yes" > /etc/xl2tpd/xl2tpd.conf

echo "require-mschap-v2
ms-dns 8.8.8.8
ms-dns 8.8.4.4
auth
mtu 1200
mru 1000
crtscts
hide-password
modem
name l2tpd
proxyarp
lcp-echo-interval 30
lcp-echo-failure 4" > /etc/ppp/options.xl2tpd
echo "# username  l2tpd password  *" >> /etc/ppp/chap-secrets
echo ""
echo ""
echo "To Do:"
echo ""
echo "- Add users: /etc/ppp/chap-secrets"
echo "Restart the software: /etc/init.d/ipsec restart;  /etc/init.d/xl2tpd restart"
